using System;
using System.ComponentModel.DataAnnotations;

namespace Practice.Models
{
    public class Thing
    {
        public int Id {get;set;}
        
        [Required, MinLength(1), MaxLength(20)]
        public string Name {get; set;}

    }
}