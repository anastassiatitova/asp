using Practice2.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Practice2.Data
{
    public class Practice2DbContext : IdentityDbContext
    {
        public Practice2DbContext(DbContextOptions<Practice2DbContext> options) : base(options) { }

        public DbSet<Article> Articles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Blog.sqlite");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // On event model creating
            base.OnModelCreating(modelBuilder);
        }
    }
}