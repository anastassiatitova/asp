using MyFriends.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MyFriends.Data
{
    public class FriendConfiguration : IEntityTypeConfiguration<Friend>
    {
             public void Configure(EntityTypeBuilder<Friend> builder)
        {
            // Fluent API can be used here to specify additional requirements
            builder.Property(p => p.Age).HasColumnName("Age");
        }
    }
}