using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Razor.Models;
using Razor.Data;
using Razor.Services;

namespace Razor.Pages
{
    public class DetailsModel : PageModel
    {
        private readonly ILogger<DetailsModel> _logger;
        private readonly IEmployeeRepository employeeRepository;
        public DetailsModel(ILogger<DetailsModel> logger, IEmployeeRepository employeeRepository)
        {
            _logger = logger;
            this.employeeRepository = employeeRepository;
        }
        public Employee Employee { get; set; }

        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }

        [TempData]
        public string Message { get; set; }

        public IActionResult OnGet()
        {
            Employee = employeeRepository.GetEmployee(Id);
            if (Employee == null)
            {
                return RedirectToPage("/NotFound");
            }
            return Page();
        }
    }
}
