using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Razor.Models;
using Razor.Services;

namespace Razor.Pages.Employees
{
    public class DeleteModel : PageModel
    {

        private readonly ILogger<EditModel> _logger;
        private readonly IEmployeeRepository employeeRepository;
        private readonly IWebHostEnvironment webHostEnvironment;

        public DeleteModel(ILogger<EditModel> logger, IEmployeeRepository employeeRepository, IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;
            this.employeeRepository = employeeRepository;
            this.webHostEnvironment = webHostEnvironment;
        }


        [BindProperty]
        public Employee Employee { get; set; }

        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }
        public IActionResult OnGet()
        {
            Employee = employeeRepository.GetEmployee(Id);
            if (Employee == null)
            {
                return RedirectToPage("/NotFound");
            }
            return Page();
        }

        public IActionResult OnPost()
        {
            Employee  deletedEmployee = employeeRepository.Delete(Employee.Id);
            if (deletedEmployee == null)
            {
                return RedirectToPage("/NotFound");
            }
            return RedirectToPage("Index");
        }
    }
}
