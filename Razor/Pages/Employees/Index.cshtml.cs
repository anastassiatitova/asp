using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Razor.Models;
using Razor.Data;
using Razor.Services;

namespace Razor.Page
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IEmployeeRepository employeeRepository;
        public IndexModel(ILogger<IndexModel> logger, IEmployeeRepository employeeRepository)
        {
            _logger = logger;
            this.employeeRepository = employeeRepository;
        }
        public IEnumerable<Employee> Employees { get; set; }
        public void OnGet()
        {

            Employees = employeeRepository.GetAllEmployees();
        }

    }
}
