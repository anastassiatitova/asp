using Razor.Models;
using Razor.Data;
using System.Collections.Generic;
using System.Linq;

namespace Razor.Services
{
    public class MockEmployeeRepository : IEmployeeRepository
    {
        private List<Employee> _employeeList;

        public MockEmployeeRepository()
        {
            _employeeList = new List<Employee>(){
                new Employee() { Id = 1, Name = "Orangey", Department = Dept.HR,Email = "mary@abc.com", PhotoPath="p1.jpg" },
                new Employee() { Id = 2, Name = "John", Department = Dept.IT, Email = "john@abc.com", PhotoPath="p2.jpg" },
                new Employee() { Id = 3, Name = "Sam", Department = Dept.IT, Email = "sam@abc.com" }
            };

        }

        public Employee Add(Employee newEmployee)
        {
           newEmployee.Id = _employeeList.Max(e => e.Id) + 1;
           _employeeList.Add(newEmployee);
           return newEmployee;
        }

        public Employee Delete(int id)
        {
            Employee employeeToDelete = _employeeList.FirstOrDefault(e => e.Id == id);
            if (employeeToDelete != null)
            {
                _employeeList.Remove(employeeToDelete);
            }
            return employeeToDelete;
        }

        public IEnumerable<Employee> GetAllEmployees()
        {
            return _employeeList;
        }

        public Employee GetEmployee(int id)
        {
            return _employeeList.FirstOrDefault(e => e.Id == id);
        }

        public Employee Update(Employee updatedEmployee)
        {
           Employee employee = _employeeList.FirstOrDefault(e => e.Id == updatedEmployee.Id);
           if(employee != null){
               employee.Name = updatedEmployee.Name;
               employee.Email = updatedEmployee.Email;
               employee.Department = updatedEmployee.Department;
               employee.PhotoPath = updatedEmployee.PhotoPath;
           }
           return employee;
        }
    }
}