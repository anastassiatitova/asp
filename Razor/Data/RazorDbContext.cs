using Razor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Razor.Data
{
    public class RazorDbContext : IdentityDbContext
    {
        public RazorDbContext(DbContextOptions<RazorDbContext> options) : base(options) { }
        public DbSet<Employee> Employees { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Razor.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
              // On event model creating
            base.OnModelCreating(modelBuilder);
        }
    }
}