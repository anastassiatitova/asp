using System;

namespace Razor.Models
{
    public enum Dept
    {
        None,
        HR,
        IT,
        Payroll
    }
}