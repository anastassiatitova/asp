using Quiz1Auction.Models;
using Microsoft.EntityFrameworkCore;

namespace Quiz1Auction.Data
{
    public class Quiz1AuctionDbContext : DbContext
    {
        public DbSet<Auction> Auctions { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Quiz1Auction.db");
        }
    }
}