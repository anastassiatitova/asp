using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using Quiz1Auction.Data;
using Quiz1Auction.Models;

namespace Quiz1Auction.Pages
{
    public class AuctionModel : PageModel
    {
        private readonly Quiz1AuctionDbContext db;
        public AuctionModel(Quiz1AuctionDbContext db) => this.db = db;

        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }

        [BindProperty]
        public Auction Bid { get; set; }

        public IActionResult OnGet()
        {
            Bid = db.Auctions.Find(Id);
            if (Bid == null)
            {
                return RedirectToPage("/NotFound");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {

                var AuctionFromDB = db.Auctions.FirstOrDefault(x => x.Id == Id);

                if (Bid.LastPrice <= AuctionFromDB.LastPrice)
                {
                    ModelState.AddModelError("Bid", "New bid should be higher than the old one.");
                    return Page();
                }

                AuctionFromDB.LastPrice = Bid.LastPrice;
                AuctionFromDB.LastBidderEmail = Bid.LastBidderEmail;
                await db.SaveChangesAsync();
                return RedirectToPage("/AuctionBidSuccess");
            }
            return Page();
        }
    }
}
