﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Quiz1Auction.Data;
using Quiz1Auction.Models;
using Microsoft.EntityFrameworkCore;

namespace Quiz1Auction.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly Quiz1AuctionDbContext db;

        public IndexModel(ILogger<IndexModel> logger, Quiz1AuctionDbContext db)
        {
            _logger = logger;
            this.db = db;
        }

       public List<Auction> AuctionList { get; set; } = new List<Auction>();

        public void OnGet()
        {
            AuctionList = db.Auctions.ToList();
        }
    }
}
