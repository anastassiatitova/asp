using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using Quiz1Auction.Data;
using Quiz1Auction.Models;

namespace Quiz1Auction.Pages
{
    public class NewAuctionModel : PageModel
    {
        private readonly Quiz1AuctionDbContext db;
        public NewAuctionModel(Quiz1AuctionDbContext db) => this.db = db;

        [BindProperty]
        public Auction Auction { get; set; }
        public void OnGet()
        {
        }
                
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            db.Auctions.Add(Auction);
            await db.SaveChangesAsync();

            return RedirectToPage("./NewAuctionSuccess");
        }

    }
}
