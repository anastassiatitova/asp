using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quiz1Auction.Models
{
    public class Auction
    {
        public int Id { get; set; }

        [Required, EmailAddress]
        [Display(Name = "Seller Email")]
        public string SellerEmail { get; set; }

        [Required, MinLength(2), MaxLength(100)]
        [Display(Name = "Item Name")]
        [RegularExpression(@"^[a-zA-Z0-9 ./,_()-]+$",
        ErrorMessage = "Only uppercase, lowercase, digits, spaces and: ./,_()- are allowed")]
        public string ItemName { get; set; }

        [Required, MinLength(2), MaxLength(10000)]
        [Display(Name = "Item Description")]
        public string ItemDescription { get; set; }

        [Required, Range(0, int.MaxValue)]
        [Display(Name = "Last Price")]
        public decimal LastPrice { get; set; }

        [EmailAddress]
        [Display(Name = "Last Bidder Email")]
        public string LastBidderEmail { get; set; }
    }
}